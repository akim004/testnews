'use strict';

var app = {
	config : {

	},

	init : function(config){

		$.extend(app.config, config);

	},
};


$(window).load(function() {
	app.init();
});

<?php

$config = [
	'id' => 'web-application',
	'defaultRoute' => 'main/default/index',
	'bootstrap' => [
		'log',
		'app\modules\main\Bootstrap',
		'app\modules\admin\Bootstrap',
		'app\modules\news\Bootstrap',
	],
	'modules' => [
		'news' => [
			'class' => 'app\modules\news\Module',
			'controllerNamespace' => 'app\modules\news\controllers\frontend',
			'viewPath' => '@app/modules/news/views/frontend',
		],
		'admin' => [
			'class' => 'app\modules\admin\Module',
			'modules' => [
				'news' => [
					'class' => 'app\modules\news\Module',
					'controllerNamespace' => 'app\modules\news\controllers\backend',
					'viewPath' => '@app/modules/news/views/backend',
				],
			],
		],
		'main' => [
			'class' => 'app\modules\main\Module',
			// 'configFromSource' => app\modules\config\models\common\Config::getModuleConfig(app\modules\main\Module),
		],
	],
	'components' => [
		'request' => [
			'baseUrl' => '',
			// 'csrfParam' => '_csrf-backend',
			'cookieValidationKey' => 'Vli9OQvycuma_NjoWwLzwXuiU9IFdLHA',
		],
		'config' => [
			'class' => 'app\modules\config\components\AppConfig',
		],
		/*'assetManager' => array(
            //'linkAssets' => true,
            'forceCopy' => true
        ),*/

		'user' => [
			'identityClass' => 'app\modules\user\models\common\User',
			'enableAutoLogin' => true,
			'loginUrl' => ['user/default/login'],
		],

		'urlManager' => [
			'class' => 'app\components\UrlManager',
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
			],
		],

		'errorHandler' => [
			'errorAction' => 'main/default/error',
		],
		'cache' => [
	        'class' => 'yii\caching\FileCache',
	    ],
	],
];

if (YII_ENV_DEV) {
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = 'yii\debug\Module';

	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = 'yii\gii\Module';
}

return $config;

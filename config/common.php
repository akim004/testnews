<?php

use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(
	require(__DIR__ . '/params.php'),
	require(__DIR__ . '/params-local.php')
);

return [
	'id' => 'jsk-application',
	'basePath' => dirname(__DIR__),
	'language' => 'ru-RU',
	'sourceLanguage' => 'en-US',
	// 'controllerNamespace' => 'backend\controllers',
	'bootstrap' => [],
	'modules' => [],
	'components' => [

		'authManager' => [
			'class' => 'yii\rbac\PhpManager',
			'defaultRoles' => ['user','moder','admin'], //здесь прописываем роли

			//зададим куда будут сохраняться наши файлы конфигураций RBAC
			'itemFile' => '@app/rbac/items.php',
			'assignmentFile' => '@app/rbac/assignments.php',
			'ruleFile' => '@app/rbac/rules.php'
		],

		'i18n' => [
			'translations' => [
				'app' => [
					'class' => 'yii\i18n\PhpMessageSource',
					'forceTranslation' => true,
				],
			],
		],

		'cache' => [
			'class' => 'yii\caching\FileCache',
		],

		'mailer' => [
			'class' => 'yii\swiftmailer\Mailer',
			'useFileTransport' => true,
		],

		/*'session' => [
			// this is the name of the session cookie used for login on the backend
			'name' => 'advanced-backend',
		],*/
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],

		'db' => [
			'class' => 'yii\db\Connection',
			'charset' => 'utf8',
			'dsn' => 'mysql:host=localhost;dbname=testnews',
			'username' => 'root',
			'password' => '',
			'tablePrefix' => 'tbl_',
		],

		'urlManager' => [
			'class' => 'app\components\UrlManager',
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [


				'<_module:[\w\-]+>/<_controller:[\w\-]+>/<_action:[\w\-]+>/<id:\d+>' => '<_module>/<_controller>/<_action>',
				'<_module:[\w\-]+>/<_controller:[\w\-]+>/<id:\d+>' => '<_module>/<_controller>/view',
				'<_module:[\w\-]+>' => '<_module>/default/index',
				'<_module:[\w\-]+>/<_controller:[\w\-]+>' => '<_module>/<_controller>/index',

				'<_controller:[\w\-]+>/<_action:[\w\-]+>/<id:\d+>' => '<_controller>/<_action>',
				'<_controller:[\w\-]+>/<id:\d+>' => '<_controller>/view',
				'<_controller:[\w\-]+>/<_action:[\w\-]+>' => '<_controller>/<_action>',
				'<_controller:[\w\-]+>' => '<_controller>/index',


			],
		],
	],
	'params' => $params,
];

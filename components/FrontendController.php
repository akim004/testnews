<?php

namespace app\components;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * BackendController is base class for backend controllers in DotPlant2.
 * In such controllers we should not see backend floating panel.
 * @package app\backend\components
 */
class FrontendController extends Controller
{
    public $layout = "@app/modules/main/views/layouts/main";
}
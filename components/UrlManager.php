<?php

namespace app\components;

class UrlManager extends \yii\web\UrlManager
{
    /**
     * Регистрирует роуты модуля.
     *
     * @param ModuleVersion $module
     */
    public  function registerModuleRules(\app\components\ModuleInterface $module)
    {
        $this->addRules($module::getUrlRules(), false);
    }
}
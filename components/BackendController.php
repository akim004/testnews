<?php

namespace app\components;

use Yii;
use yii\web\Controller;

/**
 * BackendController is base class for backend controllers
 * @package app\backend\components
 */
class BackendController extends Controller
{
    public $layout = "@app/modules/admin/views/layouts/main";
}
<?php

namespace app\components\grid;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class EditableColumnUpdate extends \dosamigos\grid\EditableColumn
{
    protected function renderDataCellContent($model, $key, $index)
    {

        $value = parent::renderDataCellContent($model, $key, $index);
        if (is_callable($this->editableOptions)) {
            $opts = call_user_func($this->editableOptions, $model, $key, $index);
            foreach ($opts as $prop => $v) {
                $this->options['data-' . $prop] = $v;
            }
        } elseif (is_array($this->editableOptions)) {
            foreach ($this->editableOptions as $prop => $v) {
                $this->options['data-' . $prop] = $v;
            }
        }

        $url = (array)$this->url;
        $this->options['data-url'] = Url::to($url);
        $this->options['data-pk'] = base64_encode(serialize($key));
        $this->options['data-name'] = $this->attribute;
        $this->options['data-type'] = $this->type;
        $this->options['data-value'] = isset($this->editableOptions['data-value']) ? $value : $value;

        return Html::a($value, null, $this->options);
    }
}
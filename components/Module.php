<?php

namespace app\components;

use Yii;
use yii\web\ForbiddenHttpException;

abstract class Module extends \yii\base\Module implements \app\components\ModuleInterface
{

	use \app\components\ModuleTrait;

	public function init()
    {
        parent::init();

        $this->registerConfig($this->configFromSource);
    }
}

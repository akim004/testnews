<?php

namespace app\modules\main\models;

use Yii;
use yii\data\ActiveDataProvider;

class Data extends \yii\db\ActiveRecord
{

	public static function tableName()
	{
		return '{{data}}';
	}

	public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        return $dataProvider;
    }
}

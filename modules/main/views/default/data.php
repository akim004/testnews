<?php 

use yii\helpers\Html;
?>

<?=Html::a('Преобразовать', ['/main/default/udpate-data'], ['class' => 'btn'])?>

<?=Html::a('Вернуть данные', ['/main/default/insert-data'], ['class' => 'btn'])?>

<?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'card_number',
            'card_number',
            'date',
            'volume',
            'service',
            'address_id',
        ],
    ]); ?>

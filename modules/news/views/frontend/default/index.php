<?php

use app\modules\news\models\frontend\News;
use yii\helpers\Html;

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <ul>
                <?foreach ($newsCounts as $year => $monthList) {?>
                    <li>
                        <?=Html::a($year, ['/news/default/index', 'year' => $year]);?>
                        <ul>
                            <?foreach ($monthList as $news) {?>
                                <li>
                                    <?=Html::a(News::getMonthName($news['month']).' ('.$news['count'].')', ['/news/default/index', 'month' => $news['month'], 'year' => $news['year']]);?>
                                </li>
                            <?}?>
                        </ul>
                    </li>
                <?}?>
            </ul>
            <hr>
            <ul>
                <?foreach ($newsObejctsCounts as $object) {?>
                    <li>
                        <?=Html::a(News::getObjectName($object['object_id']).' ('.$object['count'].')', ['/news/default/index', 'object' => $object['object_id']]);?>
                    </li>
                <?}?>
            </ul>
        </div>
        <div class="col-md-9">
          <?=\yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}\n{pager}",
                'itemView' => '_view',
            ]);?>
        </div>
    </div>
</div>
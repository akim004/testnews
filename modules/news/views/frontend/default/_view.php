<?php

use app\modules\news\models\frontend\News;
use yii\helpers\Html;

?>

<blockquote>
    <p>
        <?=$model->name?>
    </p>
	<b>Тема: </b><?=News::getObjectName($model->object_id);?><br>
	<b>Дата публикации: </b><?=date('d.m.Y', $model->date)?><br>
    <small>
        <?=$model->description?>
        <?=News::cropText($model->description);?>
    </small>
    <br>
    <?=Html::a('Читать далее...', ['/news/default/view', 'id' => $model->id]);?>
</blockquote>
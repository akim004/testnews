<?php

use app\modules\news\Module;
use app\modules\news\models\common\News;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="news-form">

	<?php $form = ActiveForm::begin([
		'options'=>['enctype' => 'multipart/form-data'],
	]); ?>

	<ul class="nav nav-tabs" id="myTab">
		<li class="active"><a href="#home">Основные</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active x_content" id="home">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'object_id')->dropDownList(News::getObjectsArray()) ?>
			<?= $form->field($model, 'description')->textArea(['style' => 'height:250px;'])?>

			<?
			if(!empty($model->date)){
			    $model->date = date('d.m.Y', $model->date);
			}else{
				$model->date = date('d.m.Y', time());
			}
			echo '<label class="control-label">'.$model->getAttributeLabel('date').'</label>';
			echo \kartik\date\DatePicker::widget([
				'model' => $model,
    			'attribute' => 'date',
    			'convertFormat' => true,
				'type' => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND,
				// 'name' => 'dp_3',
				// 'value' => '23-Feb-1982',
				'pluginOptions' => [
					'autoclose'=>true,
					'format' => 'dd.MM.yyyy'
				]
			]);
			?>

		</div>
	</div>
	<hr>
	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? Module::t('module', 'Create') : Module::t('module', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		<?= Html::submitButton(Module::t('module', 'apply'), ['class' => 'btn btn-primary', 'id' => 'wait']) ?>
		<?= Html::a(Module::t('module', 'cancel'), ['index'], ['class' => 'btn btn-danger']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>

<?php

use yii\db\Schema;
use yii\db\Migration;

class m151030_121122_news_table extends Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%news}}', [
			'id'          => Schema::TYPE_PK,
			'name'        => Schema::TYPE_STRING . '(255) NOT NULL',
			'description' => Schema::TYPE_TEXT . ' NULL DEFAULT NULL',
			'date'        => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
			'object_id'   => Schema::TYPE_INTEGER . '(11) NOT NULL DEFAULT 0',
		], $tableOptions);

		$this->createIndex('idx_news_name', '{{%news}}', 'name');
	}

	public function down()
	{
		$this->dropTable('{{%news}}');
	}
}

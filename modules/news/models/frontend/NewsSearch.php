<?php

namespace app\modules\news\models\frontend;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\news\models\frontend\News;

/**
 * NewsSearch represents the model behind the search form about `app\modules\news\models\News`.
 */
class NewsSearch extends News
{
    public $month;
    public $year;
    public $object;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['month', 'year', 'date'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function attributeLabels()
    {
        return[
            'month' => 'Месяц',
            'year'  => 'Год',
        ];
    }

    public function search($params)
    {
        $query = News::find();

        $this->year = Yii::$app->request->get('year');
        $this->month = Yii::$app->request->get('month');
        $this->object = Yii::$app->request->get('object');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort'=> [
                'defaultOrder' => ['date' => SORT_DESC],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'YEAR(FROM_UNIXTIME(date))'  => $this->year,
            'MONTH(FROM_UNIXTIME(date))' => $this->month,
            'object_id'                  => $this->object,
        ]);

        return $dataProvider;
    }
}

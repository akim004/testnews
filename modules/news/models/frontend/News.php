<?php

namespace app\modules\news\models\frontend;

use Yii;
use yii\helpers\Url;

class News extends \app\modules\news\models\common\News
{
	public static function getAllByYears()
	{
		$query = new \yii\db\Query;
		$news = [];

		$rows = $query->select('
					FROM_UNIXTIME(date) date,
					MONTH(FROM_UNIXTIME(date)) month,
					YEAR(FROM_UNIXTIME(date)) year,
					COUNT(*) count
				')
				->from('tbl_news')
				->groupBy('MONTH(FROM_UNIXTIME(date)), YEAR(FROM_UNIXTIME(date))')
				->all();

		foreach ($rows as $row => $value) {
			$news[$value['year']][] = $value;
		}

		return $news;
	}

	public static function getAllObjects()
	{
		$query = new \yii\db\Query;

		$obejcts = $query->select('
					object_id,
					COUNT(*) count
				')
				->from('tbl_news')
				->groupBy('object_id')
				->all();

		return $obejcts;
	}

	public static function getMonthName($month)
	{
		return \Yii::t('app', '{0, date, MMMM}', mktime(0, 0, 0, $month,10));
	}

	public static function cropText($string)
	{
		$string = strip_tags($string);
		$count = mb_strlen($string, 'UTF-8');
		$string = substr($string, 0, 256);
		$string = rtrim($string, "!,.-");
		$string = substr($string, 0, strrpos($string, ' '));

		if($count > 256){
			return $string."… ";
		}else{
			return $string;
		}
	}

	public static function getObjectName($objectId)
	{
	    $objects = self::getObjectsArray();

	    return $objects[$objectId];
	}

}

<?php

namespace app\modules\news\models\backend;

use Yii;
use app\modules\news\Module;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class News extends \app\modules\news\models\common\News
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%news}}';
	}

	public function rules()
	{
		return ArrayHelper::merge(parent::rules(), [

		]);
	}
}

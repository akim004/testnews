<?php

namespace app\modules\news\models\common;

use Yii;
use app\modules\news\Module;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property integer $
 * @property integer $updated_at
 * @property string $name
 * @property string $description
 * @property integer $date
 * @property string $slug
 * @property integer $published
 */
class News extends \yii\db\ActiveRecord
{

	const OBJECT_ONE = 1;
	const OBJECT_TWO = 2;
	const OBJECT_FOUR = 4;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%news}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'required'],
			[['description'], 'string'],
			[['name'], 'string', 'max' => 255],
			[['date'], 'filter', 'filter' => function ($value) {
				if(!preg_match("/^[\d\+]+$/",$value) && $value > 0){
					return strtotime($value);
				} else{
					return $value;
				}
			}],
			[['date', 'object_id'], 'integer'],
		];
	}

	public function getCreateUrl()
	{
		return Url::to(['news/view', 'id' => $this->id]);
	}

	public function attributeLabels()
	{
		return [
			'id'          => Module::t('module', 'id'),
			'name'        => Module::t('module', 'name'),
			'description' => Module::t('module', 'description'),
			'date'        => Module::t('module', 'date'),
			'object_id'   => Module::t('module', 'Object'),
		];
	}

	public static function getObjectsArray()
	{
		return [
			self::OBJECT_ONE  => 'Тема номер один',
			self::OBJECT_TWO  => 'Тема номер два',
			self::OBJECT_FOUR => 'Тема номер четыре',
		];
	}
}

<?php

return [
	'News'                  => 'Новости',
	'Create News'           => 'Создать новость',
	'Create'                => 'Создать',
	'Update'                => 'Редактировать',
	'Delete'                => 'Удалить',
	'Update {modelClass}: ' => 'Редактировать новость: ',

	'apply' => 'Применить',
	'cancel' => 'Отмена',

	'successfully added' => 'Новость успешно добавлена',
	'successfully changed' => 'Новость успешно изменена',

	'id'          => 'ID',
	'created_at'  => 'Создан',
	'updated_at'  => 'Обновлен',
	'name'        => 'Название',
	'description' => 'Полное описание',
	'slug'        => 'Алиас',
	'published'   => 'Статус',
	'date'        => 'Дата публикации',
	'Object'      => 'Тема',

	'Active'     => 'Активный',
	'Not active' => 'Скрытый',
];
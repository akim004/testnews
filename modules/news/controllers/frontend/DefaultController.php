<?php

namespace app\modules\news\controllers\frontend;

use Yii;
use app\modules\news\models\frontend\News;
use app\modules\news\models\frontend\NewsSearch;
use yii\web\Controller;

class DefaultController extends \app\components\FrontendController
{
    public function actionIndex()
    {
    	$newsCounts = News::getAllByYears();
    	$newsObejctsCounts = News::getAllObjects();

    	$searchModel = new NewsSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
				'newsCounts'   => $newsCounts,
				'dataProvider' => $dataProvider,
				'newsObejctsCounts' => $newsObejctsCounts,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

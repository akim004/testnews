<?php

/**
 * Command to create database table
 * yii migrate --migrationPath=@app/modules/news/migrations --interactive=0
 *
 * "kartik-v/yii2-widget-datepicker": "@dev",
 * \himiklab\sortablegrid\SortableGridBehavior
 */

namespace app\modules\news;

use Yii;

class Module extends \app\components\Module
{
    public $controllerNamespace = 'app\modules\news\controllers';

    public function init()
	{
		parent::init();
		// custom initialization code goes here
	}

	public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/news/' . $category, $message, $params, $language);
	}

	/**
	 * Возвращает список правил роутинга.
	 *
	 * @return array
	 */
	public static function getUrlRules()
	{
		return [];
	}
}

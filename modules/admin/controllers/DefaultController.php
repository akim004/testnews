<?php

namespace app\modules\admin\controllers;

use app\components\BackendController;
use yii\web\Controller;

class DefaultController extends BackendController
{
	public function actions()
	{
	    return [
	        'error' => [
	            'class' => 'yii\web\ErrorAction',
	            'view' => '@yiister/gentelella/views/error',
	        ],
	    ];
	}

    public function actionIndex()
    {
        return $this->render('index');
    }
}

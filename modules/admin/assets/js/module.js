var adminApplication = {
	config : {

	},

	init : function(config){
			/*new PNotify({
                                  title: 'Regular1 Success',
                                    text: 'That thing that you were trying to do worked!',
                                    dir: 'left',
                                    // addclass: {'dir1': 'down', 'dir2': 'right'},
                                    addclass: 'dark',
                                    styling: 'bootstrap3',

                                 });*/

		$.extend(adminApplication.config, config);

		$('.nav.side-menu .active').closest('.child_menu').show();

		$('.for-remove-function').on('fileclear', function(event) {
		    $('.removeImage').val(true);
		});

		$('#wait').on('click', function(event) {
			event.preventDefault();
			$(this).closest('form').attr('action', window.location.pathname+'?'+adminApplication.insertParam('wait', 1)).trigger('submit');
		});

		$('#myTab a').click(function (e) {
			e.preventDefault();
			$(this).tab('show');
		})

		$(document).on('click', '.pjax-link', function(event) {
			event.preventDefault();
			var url = $(this).attr('href');
			var pjaxId = $(this).attr('data-pjax-id');
			var confirmText = $(this).attr('data-confirm-text');

			if(confirmText != 'undefined'){
				if(!confirm(confirmText)){
					return false;
				}
			}
			adminApplication.updatePjax(url, pjaxId);
		});
	},

	updatePjax: function(url, pjaxId) {
		$.ajax({
			url: url,
		})
		.done(function(data) {})
		.fail(function() {})
		.always(function() {
			$.pjax.reload({container:'#'+pjaxId, timeout: 10000});
		});

	},

	insertParam: function(key, value){

			key = encodeURI(key); value = encodeURI(value);

			var kvp = document.location.search.substr(1).split('&');
			var i=kvp.length; var x; while(i--) {
				x = kvp[i].split('=');
				if (x[0]==key){
					x[1] = value;
					kvp[i] = x.join('=');
					break;
				}
			}

			if(i<0) {kvp[kvp.length] = [key,value].join('=');}
			return kvp.join('&');
			// document.location.search = kvp.join('&');
	}
}

$(window).load(function() {
	adminApplication.init();
});


/**
 * points = [
 * 		{x: 12,y: 13},
 * 		{x: 120,y: 130},
 * 		{x: 223.2,y: 52.1},
 * ]
 */
(function ( $ ) {
	$.fn.polyDraw = function(options) {

		var defaults = $.extend({},{
			maxWidth: 0,
			pointCount: 3,
			points: [],
			WhereToSaveId: 'coords',
			koof: 1,
		}, options);

		$("<div/>", {
			  "style": "position: absolute;left: 0;top: 0;right: 0;bottom: 0;",
			  "id": "holder",
		}).appendTo(this);

		// var koof=1;

		$("<div/>", {
			  "class": "btn btn-primary btn-xs",
			  "style": "position: relative;z-index: 1;",
			  "text": "Убрать точки",
			  "id": "holder-delete-pixels",
		}).appendTo(this);
		var overlay = new ShapeOverlay('holder', $(this).find('img').actual('width'), $(this).find('img').actual('height'), defaults.points, defaults.pointCount,koof);
		console.log($(this).find('img').actual('width'));
		var computePolygon = function() {
			var elem = document.getElementById(defaults.WhereToSaveId);
			var coords = [];
			for (var i in overlay.xs){
				coords.push((overlay.xs[i]/koof).toFixed(2));
				coords.push((overlay.ys[i]/koof).toFixed(2));
			}
			coords = coords.join(',');
			elem.innerHTML = coords;
			elem.value = coords;
		};
		overlay.shapechange(computePolygon);

		// кнопка удалить точки/восстановить
		$(this).find('#holder-delete-pixels').on('click', function(event) {
			event.preventDefault();
			if($(this).hasClass('show-circle')){
				$(this).toggleClass('show-circle').text('Удалить точки');
				$('circle').show();
			}else{
				$(this).toggleClass('show-circle').text('Вернуть точки');
				$('circle').hide();
			}
		});

		/* var img = new Image();
		 img.onload = function() {
		   var width = this.width;
		   var hight = this.height;

			console.log(width);
			console.log(height);
		 }
		 img.src = $(this).find('img').attr('src');

		console.log($(this).find('img').actual('width'));*/
		// console.log($(this).actual('width'));
		// console.log($(this).actual('width'));

		return this;
	}
}( jQuery ));
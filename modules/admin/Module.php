<?php

namespace app\modules\admin;

use Yii;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

class Module extends \app\components\Module
{
	public $controllerNamespace = 'app\modules\admin\controllers';

	public function init()
	{
		parent::init();
	}

	/*public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['adminka'],
                    ],
                ],
            ],
        ];
    }*/


	public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/admin/' . $category, $message, $params, $language);
	}

	/**
	 * Возвращает список правил роутинга.
	 *
	 * @return array
	 */
	public static function getUrlRules()
	{
		return [
			[
				'class' => 'yii\web\GroupUrlRule',
				'prefix' => 'admin',
				'routePrefix' => 'admin',
				'rules' => [
					'' => 'default/index',
					'<_module:[\w\-]+>/<_controller:[\w\-]+>/<id:\d+>' => '<_module>/<_controller>/view',
					'<_module:[\w\-]+>/<_controller:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => '<_module>/<_controller>/<_a>',
					'<_module:[\w\-]+>' => '<_module>/default/index',
					'<_module:[\w\-]+>/<_controller:[\w\-]+>' => '<_module>/<_controller>/index',
				],
			],
		];
	}
}

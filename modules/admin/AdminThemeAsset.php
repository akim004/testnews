<?php

namespace app\modules\admin;

use yii\web\AssetBundle;

class AdminThemeAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower/gentelella/vendors/';
    public $css = [
        'pnotify/dist/pnotify.css',
        'PACE/themes/green/pace-theme-flash.css',
    ];
    // public $cssOptions = ['position' => \yii\web\View::POS_HEAD];
    public $js = [
        'pnotify/dist/pnotify.js',
        'PACE/pace.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
    ];
}

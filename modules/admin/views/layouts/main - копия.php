<?php

/*echo '<pre>';
print_r(Yii::getAlias('@app/web/upload'));
exit();*/

use app\widgets\Alert;
use app\modules\admin\ModuleAsset;
use app\modules\admin\Module;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

ModuleAsset::register($this);

$controller = $this->context;
$module = $controller->module;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
	<div class="wrap">
		<?php
			NavBar::begin([
				'brandLabel' => Module::t('module', 'Dashboard'),
				'brandUrl' => '/admin',
				'options' => [
					'class' => 'navbar-inverse navbar-fixed-top',
				],
			]);
			echo Nav::widget([
				'options' => ['class' => 'navbar-nav navbar-right'],
				'items' => array_filter([
					[
						'label' => 'Пользователи',
						'url' => ['/admin/user/default/index'],
						'active' => $module->id === 'user',
						// 'visible' => Yii::$app->getUser()->can('moder'),
					],
					[
						'label' => 'Настройки',
						'url' => ['/admin/config/default/index'],
						'active' => $module->id === 'config',
						// 'visible' => Yii::$app->getUser()->can('moder'),
					],
					[
						'label' => 'Навигация',
						'url' => ['/admin/navigation/default/index'],
						'active' => $module->id === 'navigation',
					],
					[
						'label' => 'Стат.страницы',
						'url' => ['/admin/page/default/index'],
						'active' => $module->id === 'page',
					],
					[
						'label' => 'Новости',
						'url' => ['/admin/news/default/index'],
						'active' => $module->id === 'news',
					],
				]),
			]);
			NavBar::end();
		?>

		<div class="container">
			<?= Breadcrumbs::widget([
				'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
			]) ?>
			<?= Alert::widget() ?>
			<?= $content ?>
		</div>
	</div>

	<footer class="footer">
		<div class="container">
			<p class="pull-left">&copy; My Company <?= date('Y') ?></p>
			<p class="pull-right"><?= Yii::powered() ?></p>
		</div>
	</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

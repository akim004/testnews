<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ModuleAsset extends AssetBundle
{
	/*public $basePath = '@webroot/modules/admin';
	public $baseUrl = '@web/modules/admin';*/

	public $sourcePath = '@app/modules/admin/assets/';
	public $css = [
		'css/module.css',
	];
	public $js = [
		'js/module.js',
		'js/shape/jquery.liActualSize.js',
		'js/shape/raphael-min.js',
		'js/shape/shapeOverlay.js',
		'js/shape/polyDraw.js',
	];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];
}

<?php

return [
	'components' => [
		'db' => [
			'class' => 'yii\db\Connection',
			'charset' => 'utf8',
			'dsn' => 'mysql:host=localhost;dbname=jsk',
			'username' => 'root',
			'password' => '',
			'tablePrefix' => 'tbl_',
		],
		'mailer' => [
			'useFileTransport' => true,
		],
	],
];
